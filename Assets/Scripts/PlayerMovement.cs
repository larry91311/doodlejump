﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D playerRig;
    BoxCollider2D playerBC;

    bool OnAir=false;
    bool Collide=true;

    public float xSpeed=0;
    public float ySpeed=0;
    public float initialySpeed=0;

    public float speed=2.0f;


    
    public float jumpSpeed=10.0f;
    public float jumpdownRate=0.7f;
    [SerializeField, Range(0f, 10f)]
	float jumpHeight = 20f;
    // Start is called before the first frame update
    void Start()
    {
        playerRig=GetComponent<Rigidbody2D>();
        playerBC=GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        xSpeed=Input.GetAxis("Horizontal")*speed;
        playerRig.velocity=new Vector2(xSpeed,playerRig.velocity.y);

        // if(flyTimeCal)
        // {
        //     flyTime+=Time.deltaTime;
        //     if(flyTime>2.0f)
        //     {
        //         ySpeed-=Time.deltaTime*jumpdownRate;
        //     }
        // }

        // if(ySpeed<-5.0f)
        // {
        //     flyTimeCal=false;
        // }

        // if(ySpeed>1.0f)     //飛的時候不能撞到
        // {
        //     Collide=false;
        // }
        // else
        // {
        //     Collide=true;
        // }
        
       if(ySpeed<0)
       {
           
           OnAir=false;
       }
       
        
    }

    private void FixedUpdate() {
        // if(ySpeed>=2.5f)
        // {
        //     ySpeed-=Time.deltaTime*jumpdownRate;
        // }
        
    }

    


    bool floorJump=false;
    private float flyTime=0f;
    private bool flyTimeCal=false;
    private void OnCollisionEnter2D(Collision2D other) {
        
        
        

        if(other.gameObject.tag=="Floor")
        {
            flyTimeCal=false;
            flyTime=0f;

            floorJump=true;
            if(floorJump==true)
            {
                Jump();
            
            }
        }

    }

    private void OnCollisionExit2D(Collision2D other) {
        
        
        if(other.gameObject.tag=="Floor"&&OnAir==false)
        {
            flyTimeCal=true;
            floorJump=false;
            OnAir=true;
            
        }
    }


    private void Jump()
    {
        
        playerRig.velocity+=new Vector2(0,jumpSpeed);
    }
}
