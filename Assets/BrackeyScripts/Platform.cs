﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public float jumpForce = 10f;  //向上跳躍速度

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.relativeVelocity.y <= 0f&&collision.transform.position.y>transform.position.y)      //相對速度向下就可以撞
		{
			Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();    //取得rb
			if (rb != null)
			{
				Vector2 velocity = rb.velocity;     //令新速度
				velocity.y = jumpForce;
				rb.velocity = velocity;               //rb取得速度
			}
		}
	}
}
