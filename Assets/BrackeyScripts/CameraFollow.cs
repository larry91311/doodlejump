﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

	void LateUpdate () {
		if (target.position.y > transform.position.y)        //player比鏡頭高  就  鏡頭上移
		{
			Vector3 newPos = new Vector3(transform.position.x, target.position.y, transform.position.z);      //x原位、y為player位置、z原位
			transform.position = newPos;
		}
	}
}
