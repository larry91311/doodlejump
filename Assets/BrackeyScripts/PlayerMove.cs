﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour
{
    public float movementSpeed = 10f;

	Rigidbody2D rb;

	float movement = 0f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();            //Start取得Rig
	}
	
	// Update is called once per frame
	void Update () {
		movement = Input.GetAxis("Horizontal") * movementSpeed;       //Up設置float input
	}

	void FixedUpdate()
	{
		Vector2 velocity = rb.velocity;     //令Velocity選項方便調控
		velocity.x = movement;         //velocity水平
		rb.velocity = velocity;        //rb取得velocity
	}
}
